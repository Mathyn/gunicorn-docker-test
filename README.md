# Gunicorn + Docker test

Test project to showcase how Gunicorn and a simple Python app can be run inside a Docker container.

## Building

To build the Docker image run the following command in the root of the project:

```
docker build -t [tag] .
```

Where '[tag]' is the name of the image created.

## Running

To run the Docker image run the following command in the root of the project:

```
docker run [tag]
```

Where '[tag]' is the tag defined during the build step.

## Troubleshooting

It is possible you do not have sufficient rights to run Docker commands. In this case append 'sudo' to the commands.
