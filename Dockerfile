FROM python:3.6.6-stretch

RUN pip install gunicorn

COPY wsgi.py /

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/gunicorn", "-b", ":8000", "wsgi"]